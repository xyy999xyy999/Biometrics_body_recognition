import cv2

cap = cv2.VideoCapture("output.mp4")
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4, 4))
fgbg = cv2.createBackgroundSubtractorMOG2()

index = 0
while(1):
    ret, frame = cap.read()
    fgmask = fgbg.apply(frame)
    fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    # cv2.imshow('frame', fgmask)
    index = index + 1
    print(index)
    cv2.imwrite(str(index) + '.jpg', fgmask)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()