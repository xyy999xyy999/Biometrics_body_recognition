import cv2

# for image in images:
image = cv2.imread("020z071pf.jpg")
#cv2.imshow('Original', image)
#cv2.waitKey(0)
height, width, c = image.shape

crop_img = image[int(1/5 * height):height, int(1/4*width):int(2/3*width)]
cv2.imshow("cropped", crop_img)
cv2.waitKey(0)

bilateral_filtered_image = cv2.bilateralFilter(crop_img, 5, 175, 175)
#cv2.imshow('Bilateral', bilateral_filtered_image)
#cv2.waitKey(0)

edge_detected_image = cv2.Canny(bilateral_filtered_image, 75, 150)
#cv2.imshow('Edge', edge_detected_image)
#cv2.waitKey(0)

_, contours, _= cv2.findContours(edge_detected_image, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

contour_list = []
for contour in contours:
    approx = cv2.approxPolyDP(contour, 0.01*cv2.arcLength(contour,True),True)
    area = cv2.contourArea(contour)
    if ((len(approx) > 8) & (area > 30) ):
        contour_list.append(contour)

cv2.drawContours(edge_detected_image, contour_list,  -1, (255,0,0), 2)
cv2.imshow('Objects Detected',edge_detected_image)
cv2.waitKey(0)