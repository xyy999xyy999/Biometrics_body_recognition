import math
import glob as gb
import cv2
import numpy as np
import os

img_path = gb.glob("test/*_contour.jpg")

for file in img_path:
    image = cv2.imread(file, 0)
    _, image = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)
    # height measuring
    for h in range(image.shape[0]):
        if np.count_nonzero(image[h]) != 0:
            break
    #print("The height is {}".format(image.shape[0] - h))

    # face width measuring
    face_width = 0
    for i in np.arange(h, image.shape[0] - 1):
        potential_face = [j for j, e in enumerate(image[i]) if e != 0]
        # print(potential_face)
        x_face, y_face = potential_face[0], potential_face[-1]
        if face_width <= y_face - x_face:
            # print(face_width)
            face_width = y_face - x_face
        else:
            break
    #print("The face width is {}".format(face_width))

    #crop again
    middle_head = np.floor((x_face + y_face) / 2).astype(int)

    # waist width measuring
    waist_height = math.floor(1 / 2 * (image.shape[0] - h))
    #print(waist_height)
    potential_waist = []
    potential_waist = [j for j, e in enumerate(image[waist_height]) if e != 0]
    #print(potential_waist)
    waist_width = potential_waist[-1] - potential_waist[0]
    if waist_width >300:
        waist_width *= 0.75
        waist_width = np.floor(waist_width).astype(int)

    #print("The waist width is {}".format(waist_width))

    # neck width measuring
    grad_neck, head_height = 0, i
    for neck_height_temp in np.arange(i, np.floor((i + waist_height) / 2)).astype(int):
        potential_neck_temp = [j for j, e in enumerate(image[neck_height_temp]) if e != 0]
        if np.array(potential_neck_temp).size == 0 or middle_head - potential_neck_temp[0] == 0:
            continue
        grad_temp = (head_height - neck_height_temp) / (middle_head - potential_neck_temp[0])
        if potential_neck_temp[0] > middle_head or grad_temp > 0 or math.isinf(grad_temp):
            continue
        neck_width_temp = potential_neck_temp[-1] - potential_neck_temp[0]
        if neck_width_temp > face_width or neck_width_temp < 50:
            continue
        if grad_temp < grad_neck:
            neck_nonzero = potential_neck_temp
            grad_neck = grad_temp
            #print(grad_neck)
            neck_height = neck_height_temp
            #print(neck_height)

            neck_width = neck_width_temp

    if neck_width > 95:
        neck_width *= 0.85
        neck_width = np.floor(neck_width).astype(int)

    #print("The neck width is {}".format(neck_width))
    print("{} {} {} {}".format(image.shape[0] - h, face_width, waist_width, neck_width))


    cv2.line(image, (0, h), (image.shape[1] - 1, h), 255, 10)
    cv2.line(image, (x_face, i), (y_face, i), 255, 10)
    cv2.line(image, (potential_waist[0], waist_height), (potential_waist[-1], waist_height), 255, 10)
    cv2.line(image, (neck_nonzero[0], neck_height), (neck_nonzero[-1], neck_height), 255, 10)
    cv2.imshow(file, image)
    cv2.imwrite('a.jpg', image)
    cv2.waitKey(0)
