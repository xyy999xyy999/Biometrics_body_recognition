import glob as gb
import cv2
import numpy as np
import os
img_path = gb.glob("training/pf/*.jpg")
for file in img_path:

    boundaries = [
        ([1, 78, 57], [255, 255, 255])
    ]

    img = cv2.imread(file)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
    height, width, c = img.shape

    img = img[int(4/20 * height)+20:height, int(12/30*width):int(22/30*width)]
    for (lower, upper) in boundaries:
        # create NumPy arrays from the boundaries
        lower = np.array(lower, dtype="uint8")
        upper = np.array(upper, dtype="uint8")

        # find the colors within the specified boundaries and apply
        # the mask
        mask = cv2.inRange(img, lower, upper)

        #mask_inv = cv2.bitwise_and(mask)
        #mask_inv = cv2.GaussianBlur(mask_inv, (5, 5), 0)

        #output = cv2.bitwise_and(img, img, mask=mask)
        # show the images
        #cv2.imwrite(os.path.splitext(file)[0] + str("_mask.jpg"), output)
        #cv2.waitKey(0)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("a", gray)
    blur = cv2.GaussianBlur(gray,(3,3),0)
    #cv2.imshow("b", blur)

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (9, 9))
    dilated = cv2.dilate(blur, kernel)
    #cv2.imshow("c", dilated)

    thresh, otsu = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    #cv2.imwrite(os.path.splitext(file)[0]+str("_otsu.jpg"), otsu)
    img[otsu == 255] = [0, 0, 255]
    #cv2.namedWindow('aaa', flags=0)
    #cv2.imshow("aaa", otsu)
    #print(ret)
    max_thresh = 255
    edges = cv2.Canny(otsu,thresh,thresh*2)
    #cv2.imshow("e", edges)
    drawing = np.zeros(img.shape,np.uint8)     # Image to draw the contours
    _, contours,hierarchy = cv2.findContours(edges,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
    #_, contours, _ = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    for cnt in contours:
        hull = cv2.convexHull(cnt)
        cv2.drawContours(drawing,[cnt],0,(255,255,255),1)   # draw contours in green color
        #cv2.drawContours(drawing,[hull],0,(0,0,255),2)  # draw contours in red color
        #cv2.imshow('output',drawing)
        #cv2.imshow('input',img)
    cv2.waitKey(0)
    cv2.imwrite(os.path.splitext(file)[0]+str("_contour.jpg"), drawing)

