import csv
import numpy as np
import matplotlib.pyplot as plt

#correct_answer = [23, 23, 25, 25,27,27,29,29,31,31,33,33,35,35,37,37,39,39,41,41,87,87]
correct_answer = [23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 43, 43]
#correct_answer = [23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 38, 40, 41, 42, 87, 88]

plt.interactive(False)

def loadcsvfile2(file):
    tmp = np.loadtxt(file, dtype=np.str, delimiter=",")
    name = tmp[1:, 0]
    data = tmp[1:,1:5].astype(np.int)
    label = tmp[1:,1:9].astype(np.float)
    return name, data, label


training_name, training_data, training_label = loadcsvfile2('training.csv')
cv_name, cv_data, cv_label = loadcsvfile2('CV.csv')
hand_name, hand_data, hand_label = loadcsvfile2('byhand.csv')

# print(training_label, hand_label)

def Euc_Distance(training, test, weight):
    total = []
    for i in test:
        distance = []
        for j in training:
            temp = 0
            temp_trianing, temp_test = i * weight, j * weight
            for k in range(len(i)):
                temp += np.sqrt(np.abs(np.square(temp_test[k]) - np.square(temp_trianing[k])))
            temp /= sum(weight)
            if len(i) >= 5:
                if i[4] != j[4]:
                    temp += 999
                elif i[5] != j[5]:
                    temp += 100
                elif i[6] != j[6]:
                    temp += 20
                elif i[7] != j[7]:
                    temp += 50
            distance.append(temp)

        total.append(distance)
    return total

def Chi_Sq_Distance(training, test, weight):
    total = []
    for i in test:
        distance = []
        for j in training:
            temp = 0
            temp_trianing, temp_test = i * weight, j * weight

            for k in range(len(i)):
                if temp_trianing[k] == 0:
                    temp += 0
                else:
                    temp += np.square(temp_test[k]-temp_trianing[k]) / temp_trianing[k]
            temp /= sum(weight)
            if len(i) >= 5:
                if i[4] != j[4]:
                    temp += 999
                elif i[5] != j[5]:
                    temp += 100
                elif i[6] != j[6]:
                    temp += 20
                elif i[7] != j[7]:
                    temp += 50

            distance.append(temp)

        total.append(distance)
    return total








##### Pure Hand Data
measure_weight = [10, 10, 1, 2] # Hand data weight

hand_chi_distance = Chi_Sq_Distance(training_data, hand_data, measure_weight)
hand_chi_distance /= np.max(hand_chi_distance) - np.min(hand_chi_distance)
hand_euc_distance = Euc_Distance(training_data, hand_data, measure_weight)
hand_euc_distance /= np.max(hand_euc_distance) - np.min(hand_euc_distance)

hand_chi_true_distance = []
hand_chi_false_distance = []
hand_euc_true_distance = []
hand_euc_false_distance = []

for i in range(22):
    for j in range(88):
        if j == correct_answer[i] or j == correct_answer[i] + 1:
            hand_chi_true_distance.append(hand_chi_distance[i][j])
            hand_euc_true_distance.append(hand_euc_distance[i][j])
        else:
            hand_chi_false_distance.append(hand_chi_distance[i][j])
            hand_euc_false_distance.append(hand_euc_distance[i][j])

f = plt.figure(1)
plt.hist(hand_chi_false_distance, bins='auto', label='distance_to_false', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.hist(hand_chi_true_distance, bins='auto', label='distance_to_correct', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.legend(loc='upper right')
# plt.show()

g = plt.figure(2)
plt.hist(hand_euc_false_distance, bins='auto', label='distance_to_false', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.hist(hand_euc_true_distance, bins='auto', label='distance_to_correct', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.legend(loc='upper right')
# plt.show()


hand_chi_rank = []
hand_euc_rank = []
for i in hand_chi_distance:
    hand_chi_rank.append(np.argsort(i))

for i in hand_euc_distance:
    hand_euc_rank.append(np.argsort(i))

with open('test_chi.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(",")
    for i in hand_chi_rank:
        spamwriter.writerow(i)
with open('test_euc.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(",")
    for i in hand_euc_rank:
        spamwriter.writerow(i)

# chi_rank_n
chi_rank1 = []
for i in range(22):
    for j in range(20):
        if hand_chi_rank[i][j] == correct_answer[i] or hand_chi_rank[i][j] == correct_answer[i]+1:
            chi_rank1.append(i)
print(chi_rank1)

# euc_rank_n
euc_rank1 = []
for i in range(22):
    for j in range(20):
        if hand_euc_rank[i][j] == correct_answer[i] or hand_euc_rank[i][j] == correct_answer[i] +1:
            euc_rank1.append(i)
print(euc_rank1)

##### Pure CV Data
measure_weight = [10, 10, 1, 2] # Hand data weight

cv_chi_distance = Chi_Sq_Distance(training_data, cv_data, measure_weight)
cv_chi_distance /= np.max(cv_chi_distance) - np.min(cv_chi_distance)
cv_euc_distance = Euc_Distance(training_data, cv_data, measure_weight)
cv_euc_distance /= np.max(cv_euc_distance) - np.min(cv_euc_distance)

cv_chi_true_distance = []
cv_chi_false_distance = []
cv_euc_true_distance = []
cv_euc_false_distance = []

for i in range(22):
    for j in range(88):
        if j == correct_answer[i] or j == correct_answer[i] + 1:
            cv_chi_true_distance.append(cv_chi_distance[i][j])
            cv_euc_true_distance.append(cv_euc_distance[i][j])
        else:
            cv_chi_false_distance.append(cv_chi_distance[i][j])
            cv_euc_false_distance.append(cv_euc_distance[i][j])

f = plt.figure(5)
plt.hist(cv_chi_false_distance, bins='auto', label='distance_to_false', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.hist(cv_chi_true_distance, bins='auto', label='distance_to_correct', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.legend(loc='upper right')
# plt.show()

g = plt.figure(6)
plt.hist(cv_euc_false_distance, bins='auto', label='distance_to_false', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.hist(cv_euc_true_distance, bins='auto', label='distance_to_correct', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.legend(loc='upper right')
# plt.show()

cv_chi_rank = []
cv_euc_rank = []

for i in hand_chi_distance:
    cv_chi_rank.append(np.argsort(i))

for i in hand_euc_distance:
    cv_euc_rank.append(np.argsort(i))

with open('test_chi_cv.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(",")
    for i in hand_chi_rank:
        spamwriter.writerow(i)
with open('test_euc_cv.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(",")
    for i in hand_euc_rank:
        spamwriter.writerow(i)

# chi_rank_n
chi_rank1 = []
for i in range(22):
    for j in range(20):
        if cv_chi_rank[i][j] == correct_answer[i] or cv_chi_rank[i][j] == correct_answer[i] + 1:
            chi_rank1.append(i)
print(chi_rank1)

# euc_rank_n
euc_rank1 = []
for i in range(22):
    for j in range(20):
        if cv_euc_rank[i][j] == correct_answer[i] or cv_euc_rank[i][j] == correct_answer[i] + 1:
            euc_rank1.append(i)
print(euc_rank1)


##### Pure Hand Data + Soft biometrics
measure_weight_soft = [10, 4, 1, 2, 1, 1, 1, 1] # Hand data weight


hand_chi_distance = Chi_Sq_Distance(training_label, hand_label, measure_weight_soft)
# hand_chi_distance /= np.max(hand_chi_distance) - np.min(hand_chi_distance)
hand_euc_distance = Euc_Distance(training_label, hand_label, measure_weight_soft)
# hand_euc_distance /= np.max(hand_euc_distance) - np.min(hand_euc_distance)

# print(hand_chi_distance[0], hand_euc_distance[0])
hand_chi_true_distance = []
hand_chi_false_distance = []
hand_euc_true_distance = []
hand_euc_false_distance = []

for i in range(22):
    for j in range(88):
        if j == correct_answer[i] or j == correct_answer[i] + 1:
            hand_chi_true_distance.append(hand_chi_distance[i][j])
            hand_euc_true_distance.append(hand_euc_distance[i][j])
        else:
            hand_chi_false_distance.append(hand_chi_distance[i][j])
            hand_euc_false_distance.append(hand_euc_distance[i][j])

f = plt.figure(3)
plt.hist(hand_chi_false_distance, bins='auto', label='distance_to_false', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.hist(hand_chi_true_distance, bins='auto', label='distance_to_correct', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.legend(loc='upper right')
# plt.show()

g = plt.figure(4)
plt.hist(hand_euc_false_distance, bins='auto', label='distance_to_false', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.hist(hand_euc_true_distance, bins='auto', label='distance_to_correct', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.legend(loc='upper right')
# plt.show()

hand_chi_rank = []
hand_euc_rank = []
for i in hand_chi_distance:
    hand_chi_rank.append(np.argsort(i))

for i in hand_euc_distance:
    hand_euc_rank.append(np.argsort(i))

with open('test_chi_soft.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(",")
    for i in hand_chi_rank:
        spamwriter.writerow(i)
with open('test_euc_soft.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(",")
    for i in hand_euc_rank:
        spamwriter.writerow(i)

# chi_rank_n
chi_rank1 = []
for i in range(22):
    for j in range(20):
        if hand_chi_rank[i][j] == correct_answer[i] or  hand_chi_rank[i][j] == correct_answer[i] + 1:
            chi_rank1.append(i)
print(chi_rank1)

# euc_rank_n
euc_rank1 = []
for i in range(22):
    for j in range(20):
        if hand_euc_rank[i][j] == correct_answer[i] or hand_euc_rank[i][j] == correct_answer[i] +1:
            euc_rank1.append(i)
print(euc_rank1)

##### Pure CV Data + Soft biometrics
measure_weight_soft = [10, 4, 1, 2, 0, 0, 0, 0] # Hand data weight


cv_chi_distance = Chi_Sq_Distance(training_label, cv_label, measure_weight_soft)
hand_chi_distance /= np.max(hand_chi_distance) - np.min(hand_chi_distance)
cv_euc_distance = Euc_Distance(training_label, cv_label, measure_weight_soft)
hand_euc_distance /= np.max(hand_euc_distance) - np.min(hand_euc_distance)

# print(cv_chi_distance[0], hand_euc_distance[0])
cv_chi_true_distance = []
cv_chi_false_distance = []
cv_euc_true_distance = []
cv_euc_false_distance = []

for i in range(22):
    for j in range(88):
        if j == correct_answer[i]:# or j == correct_answer[i] + 1:
            cv_chi_true_distance.append(cv_chi_distance[i][j])
            cv_euc_true_distance.append(cv_euc_distance[i][j])
        else:
            cv_chi_false_distance.append(cv_chi_distance[i][j])
            cv_euc_false_distance.append(cv_euc_distance[i][j])

print(len(cv_chi_true_distance))
print(len(cv_chi_false_distance))
f = plt.figure(7)
plt.hist(hand_chi_false_distance, bins='auto', label='distance_to_false', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.hist(hand_chi_true_distance, bins='auto', label='distance_to_correct', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.legend(loc='upper right')
# plt.show()

g = plt.figure(8)
plt.hist(hand_euc_false_distance, bins='auto', label='distance_to_false', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.hist(hand_euc_true_distance, bins='auto', label='distance_to_correct', density = 1, stacked = True, alpha=0.5, histtype='stepfilled')
plt.legend(loc='upper right')
# plt.show()

cv_chi_rank = []
cv_euc_rank = []
for i in cv_chi_distance:
    cv_chi_rank.append(np.argsort(i))

for i in cv_euc_distance:
    cv_euc_rank.append(np.argsort(i))

with open('test_chi_cv_soft.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(",")
    for i in cv_chi_rank:
        spamwriter.writerow(i)
with open('test_euc_cv_soft.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(",")
    for i in cv_euc_rank:
        spamwriter.writerow(i)

# chi_rank_n
chi_rank1 = []
for i in range(22):
    for j in range(1):
        if cv_chi_rank[i][j] == correct_answer[i] :#or cv_chi_rank[i][j] == correct_answer[i] +1:
            chi_rank1.append(i)
print(chi_rank1)

# euc_rank_n
euc_rank1 = []
for i in range(22):
    for j in range(1):
        if cv_euc_rank[i][j] == correct_answer[i] :#or cv_euc_rank[i][j] == correct_answer[i] + 1:
            euc_rank1.append(i)
print(euc_rank1)


found = False
# max_dist = int((np.max(distance_to_false)))
thresholds = np.arange(0.0, 150, 0.1)
threshold_at_eer = []
frr = np.zeros((len(thresholds)))
far = np.zeros((len(thresholds)))
for i in range(len(thresholds)):
    threshold = thresholds[i]
    false_rejects = 0
    false_accepts = 0
    true_positive = 0
    true_negative = 0
    for j in range(22):
        if cv_chi_true_distance[j] > threshold:
            false_rejects +=1
        else:
            true_positive +=1
        for fake in cv_chi_false_distance:
            if fake <= threshold:
                false_accepts +=1
            else:
                true_negative +=1
    frr[i] = false_rejects/(true_positive+false_rejects)
    far[i] = false_accepts/(false_accepts+true_negative)
    if  abs(frr[i] - far[i]) < 0.08:
        eer = false_rejects/(true_positive+false_rejects)
        threshold_at_eer.append(threshold)
        print('eer=',eer,'threshold_at_eer=',threshold_at_eer[::-1])
        found = True
if not found:
    print('eer not found!!!')

plt.figure()
#plt.plot(far,color='blue', linewidth=2)

plt.xlabel('FRR')
plt.ylabel('FAR')
plt.plot(thresholds,far,color='blue', linewidth=2,label='FAR')
plt.plot(thresholds,frr,color='red', linewidth=2,label='FRR')
plt.legend(loc='center right')
plt.xlabel('Thresholds')
plt.ylabel('%(EER = '+str(round(eer,2)) + ')')
plt.show()